<?php
	require_once('scripts/general.php');
?>
<html>
	<head>
		<title>QA Tool - Login</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta http-equiv="cache-control" content="no-cache">
		<link rel="icon" type="image/x-icon" href="favicon.ico" />
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
	</head>
	<body>
		<div id="top"><a href="index.php"><img src="pics/logo.gif><span id="system_name">QA TOOL</span></a></div>
		<div id="content">
			<form name="login" action="scripts/login.php" method="post">
				<?php
					if(isset($_GET['err'])){
						echo '<span class="error">';
						switch($_GET['err']){
						case 1: echo 'The email or password are wrong.'; break;
						case 2: echo 'The e-mail field should not be empty.'; break;
						case 3: echo 'The e-mail field should not have more than 60 characters.'; break;
						case 4: echo 'The password field should not be empty.'; break;
						case 5: echo 'The password field should not have more than 20 characters.'; break;
						}
						echo '</span>';
					} else if(isset($_GET['logout'])){
					 echo '<span class="alert">You\'ve logged out.</span><br />';
					} 
				?>
				Please, insert your e-mail and password<br />
				<table>
					<tr><td>E-mail:</td><td><input type="text" name="email" /></td></tr>
					<tr><td>Password:</td><td><input type="password" name="password" /></td></tr>
				</table>
				<input type="submit" value="Login" />
			</form>
			<a href="forgot.php">Forgot my password</a>
		</div>
		<div id="rodape">
			<span id="lbl_criadoPor">Created by</span>: Luan Rodrigues - <a href="mailto: admin@qatool.com">admin@qatool.com</a>
			<br /><span id="lbl_versao">Version</span>: <?php echo $VERSION?>
		</div>
	</body>
<html>
