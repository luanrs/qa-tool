<?php
	require_once('scripts/general.php');
?>
<html>
	<head>
		<title>QA Tool - Forgot my password</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta http-equiv="cache-control" content="no-cache">
		<link rel="icon" type="image/x-icon" href="favicon.ico" />
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
	</head>
	<body>
		<div id="top"><a href="index.php"><img src="pics/logo.gif"><span id="system_name">QA TOOL</span></a></div>
		<div id="content">
			<?php if(!isset($_GET['changed']) and !isset($_GET['emailerror']) and !isset($_GET['usererror'])) { ?>
				Please insert your e-mail, we'll send you an e-mail with a new password.
				<form name="changepass" action="scripts/forgot.php" method="post">
					<table>
						<tr><td>Email:</td><td><input type="text" name="email" /></td></tr>
					</table>
					<input type="submit" value="Submit" />
				</form>
			<?php } else {
				if(isset($_GET['changed'])){
					echo '<span class="alert"><p>Your password has been reseted, please check your e-mail to get the new one.</p><p><a href="index.php">Go back</a></p></span>';
				} else if(isset($_GET['emailerror'])){ 
					echo '<span class="error"><p>Sorry, but we can\'t send you an e-mail with your new password, please check if your e-mail is correct and try again.</p><p><a href="forgot.php">Go back</a></p></span>';
				} else if (isset($_GET['usererror'])){
					echo '<span class="error"><p>Your e-mail is wrong.</p><p><a href="forgot.php">Go back</a></p></span>';
				}
				}
			?>	
		</div>
		<div id="rodape">
			<span id="lbl_criadoPor">Created by</span>: Luan Rodrigues - <a href="mailto: admin@qatool.com">admin@qatool.com</a>
			<br /><span id="lbl_versao">Version</span>: <?php echo $VERSION?>
		</div>
	<body>
</html>
