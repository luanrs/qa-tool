<?php
    require_once('ses.php');

    session_unset();
    setcookie('PHPSESSID','',time() -3600);
    session_destroy();
    
    header('Location: ../login.php?logout');
?>