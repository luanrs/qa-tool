// This var will load the files, if it exists, from the content/languages folder.
// These files contains the localization variables for UI. 
languages = ['en-US'];

// This var will load the lists configurated, they're under the content/lists folder, excepet for the delimiter folder.
lists = [
	'prod_amb_new',
	'prod_amb_clear',
	'prod_amb_fix',
	'prod_amb_stg',
	'prod_amb_swap',
	'prod_amb_update',
	'prod_go_live',
	'prod_sync_check',
	'prod_generate_csr',
	'qa_simple_review'
];

// This var will configure the options that will be loaded by default.
defaultLanguage = 'en-US';
defaultList = '';
defaultDelimiter = '';
loadOnStart = true;
hintOnStart = false;