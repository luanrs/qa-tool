<?php
	require_once('scripts/ses.php');
	if($_SESSION['logged'] == false){header('Location: login.php');}
	require_once('scripts/general.php');
?>
<html>
	<head>
		<title>QA Tool - Change password</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta http-equiv="cache-control" content="no-cache">
		<link rel="icon" type="image/x-icon" href="favicon.ico" />
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
	</head>
	<body>
		<div id="top"><a href="index.php"><img src="pics/logo.gif"><span id="system_name">QA TOOL</span></a>
			<span id="logged-status">
				<span id="">You're logged as</span> <span class="user"><?php echo $_SESSION['email']; ?></span><br />
				<a href="scripts/logout.php">Logout</a> - <a href="index.php">QA List</a>
			</span>
		</div>
		<div id="content">
			<form name="changepass" action="scripts/changepass.php" method="post">
				<table>
					<tr><td>Password:</td><td><input type="password" name="opassword" /></td></tr>
					<tr><td>New password:</td><td><input type="password" name="npassword" /></td></tr>
					<tr><td>Confirm new password:</td><td><input type="password" name="cnpassword" /></td></tr>
				</table>
				<input type="submit" value="Change" />
				<?php 
					if(isset($_GET['ERR'])){
						echo '<br /><br /><span class="error">';
						switch($_GET['ERR']){
						case 1: echo 'The old password is wrong.'; break;
						case 2: echo 'The new password and its confirmation should be equal.'; break;
						case 3: echo 'The new password should not be the same as the old password.'; break;
						case 4: echo 'The new password should not be empty.'; break;
						case 5: echo 'The new password should not have more than 20 characters.'; break;
						}
						echo '</span>';
					}
					if(isset($_GET['SUCCESS']) && $_GET['SUCCESS'] == true){
						echo '<br /><br /><span class="alert">Password changed successfully</span>';
					}
				?>
			</form>
		</div>
		<div id="rodape">
			<span id="lbl_criadoPor">Created by</span>: Luan Rodrigues - <a href="mailto: admin@qatool.com">admin@qatool.com</a>
			<br /><span id="lbl_versao">Version</span>: <?php echo $VERSION?>
		</div>
	<body>
</html>
