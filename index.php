﻿<?php
	require_once('scripts/ses.php');
	if($_SESSION['logged'] == false){header('Location: login.php');}
	require_once('scripts/general.php');
?>
<html>
	<head>
		<title>QA Tool</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta http-equiv="cache-control" content="no-cache">
		<link rel="icon" type="image/x-icon" href="favicon.ico" />
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
		<script type="text/javascript" src="config.js"></script>
		<script type="text/javascript" src="js/get.js"></script>
		<script type="text/javascript" src="js/classes.js"></script>
		<script type="text/javascript" src="js/functions.js"></script>
		<script type="text/javascript" src="js/init.js"></script>
	</head>
	
	<body onload="inicializador()">
	<div id="top"><a href="index.php"><img src="pics/logo.gif"><span id="system_name">QA TOOL</span></a>
		<span id="logged-status" style="float: right;">
			<span id="timer_bar" style="margin-right: 50px;">
				<script type="text/javascript" src="js/timer.js"></script>
				<script type="text/javascript">
					function startCounter(){timer.setContainer(document.getElementById('ctimer')); timer.start();}
				</script>
				<span id="ctimer">0:00:00</span>
				<span id="start" onClick="startCounter();" style="font-weight: bold; cursor:pointer;">Start</span>
				<span id="stop" onClick="timer.stop();" style="font-weight: bold; cursor:pointer;">Stop</span>
				<span id="reset" onClick="timer.reset();" style="font-weight: bold; cursor:pointer;">Reset</span>
			</span>
			<span id="">You're logged as</span> <span class="user"><?php echo $_SESSION['email']; ?></span><br />
			<a href=# onClick="abreMenuPrincipal()">Tool Menu</a> - <a href="scripts/logout.php">Logout</a> - <a href="changepass.php">Change password</a>
		</span>
	</div>
	<div id="qabody">
		<h1 id="tit_pagina">Checklist</h1>
		
		<div>
			<span id="btn_secoesExibir" class="exibirTodos smallLink">Show all sections</span> <span id="dmr_exibirSecoes" class="divisor">-</span> <span id="btn_secoesEsconder" class="exibirTodos smallLink">Hide all sections</span> |
			<span id="btn_dicasExibir" class="exibirTodos smallLink">Show all hints</span> <span id="dmr_exibirDicas" class="divisor">-</span> <span id="btn_dicasEsconder" class="exibirTodos smallLink">Hide all hints</span> |
			<span id="btn_comentariosExibir" class="exibirTodos smallLink">Show all comments</span> <span id="dmr_exibirComentarios" class="divisor">-</span> <span id="btn_comentariosEsconder" class="exibirTodos smallLink">Hide all comments</span>
		</div>
		<hr />
		
		<div id="conteudo"></div>
		
		<div id="fundoNegro"></div>
		
		<div id="mnu_topo"><span id="btn_fecharTopo">&nbsp;</span></div>
		<div id="mnu_base">&nbsp;</div>
		
		<div id="mnu_principal" class="menu">
			<span id="tit_mnu_principal"></span>
			<hr />
			<span>
				<span id="lbl_idiomaSelecionado">Selected language</span>: <span id="lbl_idiomaSelecionadoValor"></span>
				<br />
				<span id="lbl_listaSelecionada">Selected list</span>: <span id="lbl_listaSelecionadaValor"></span>
				<br />
				<span id="lbl_delimitadorSelecionado">Selected delimiter</span>: <span id="lbl_delimitadorSelecionadoValor"></span>
			</span>
			<ul>
				<li><a href="#" id="btn_relatorio" class="button">Generate report</a><span class="button_corner">&nbsp;</span></li>
				<li><a href="#" id="btn_configuracoes" class="button">Mudar configurações</a><span class="button_corner">&nbsp;</span></li>
				<li><a href="#" id="btn_salvar" class="button">Salvar Checklist</a>
				<li><a href="#" id="btn_carregar" class="button">Carregar Checklist</a>
				<li><a href="#" id="btn_ajuda" class="button">Ajuda</a><span class="button_corner">&nbsp;</span></li>
				<li><a href="#" id="btn_fechar" class="button">Fechar</a><span class="button_corner">&nbsp;</span></li>
			</ul>
		</div>
		
		<div id="mnu_configuracoes" class="menu">
			<span id="tit_mnu_configuracoes"></span> <br />
			<hr />
			<div id="cmb_idioma">
				<span id="lbl_idioma">Idioma</span><br />
				<select disabled="disabled">
					<option value="0">Selecione</option>
				</select>
			</div>
			
			<div id="cmb_lista">
				<span id="lbl_lista">Lista</span><br />
				<select disabled="disabled">
					<option value="0">Selecione</option>
				</select>
			</div>
			
			<div id="cmb_delimitador">
				<span id="lbl_delimitador">Delimitador</span><br />
				<select disabled="disabled">
					<option value="0">Selecione</option>
				</select>
			</div>
			
			<br clear="all"/>

			<a href="#" id="btn_mudar" class="button">Mudar</a><span class="button_corner">&nbsp;</span>
			<a href="#" id="btn_resetar" class="button">Resetar</a><span class="button_corner">&nbsp;</span>
			<br />
			<div class="botoesMenu">
				<a href="#" id="btn_voltar_mnu_configuracoes" class="button">Voltar</a><span class="button_corner">&nbsp;</span>
				<a href="#" id="btn_fechar_mnu_configuracoes" class="button">Fechar</a><span class="button_corner">&nbsp;</span>
			</div>
		</div>
		
		<div id="mnu_relatorio" class="menu">
			<span id="tit_mnu_relatorio"></span>
			<hr />
			<div id="conteudo_relatorio"></div>
			<div class="botoesMenu">
				<a href="#" id="btn_voltar_mnu_relatorio" class="button">Voltar</a><span class="button_corner">&nbsp;</span>
				<a href="#" id="btn_fechar_mnu_relatorio" class="button">Fechar</a><span class="button_corner">&nbsp;</span>
			</div>
		</div>
		
		<div id="mnu_dica" class="menu">
			<span id="tit_mnu_dica"></span>
			<hr />
			<div>
				<span id="textoMenuDica">Para ajuda, pressione Ctrl + Shift + A</span><br />
				<input type="checkbox" id="cbx_dica" /> <span id="textoSelecionarMenuDica">Marque essa opção para desativar essa mensagem ao iniciar</span>
			</div>
			<div class="botoesMenu">
				<a href="#" id="btn_voltar_mnu_dica" class="button">Voltar</a><span class="button_corner">&nbsp;</span>
				<a href="#" id="btn_fechar_mnu_dica" class="button">Fechar</a><span class="button_corner">&nbsp;</span>
			</div>
		</div>
		
		<div id="mnu_ajuda" class="menu">
			<span id="tit_mnu_ajuda"></span>
			<hr />
			<div id="textoAjuda">
			<span id="atalhos">Atalhos</span>
			<ul>
				<li>Ctrl + A: <span id="atalho1">Abre/Fecha todos os campos de comentários</span></li>
				<li>Ctrl + D: <span id="atalho2">Abre/Fecha todos os campos de dicas</span></li>
				<li>Ctrl + E: <span id="atalho3">Expande/Contrai todas as seções</span></li>
				<li>Ctrl + R: <span id="atalho4">Gera e abre/fecha relatório</span></li>
				<li>Ctrl + M: <span id="atalho5">Abre/Fechar menu principal</span></li>
				<li>Ctrl + Alt + A: <span id="atalho6">Abre/Fechar esse menu de ajuda</span></li>				
				<br />
				<li>Shift: <span id="atalho7">Clique em uma opção, Abre/Fecha campo de comentário da opção</span></li>
				<li>Ctrl: <span id="atalho8">Clique em uma opção, Abre/Fecha campo de dica da opção</span></li>
				<br />
				<li>ESC: <span id="atalho9">Com um menu aberto, fecha o menu</span></li>
				<br />
				<span id="atalhoTexto1">É possível configurar o self-qa utilizando a URL. Basta colocar uma '?' e os atributos, lang para idioma, type para lista e del para delimitador. Quando for utilizar mais de um atributo, usar '&' entre eles.</span>
				<br /><span id="atalhoExemplo1">Exemplo: www.luanrs.com/self_qa/self_qa.html?<strong>lang</strong>=pt-BR&amp;<strong>type</strong>=bo2.0&amp;<strong>del</strong>=AR</span>
			</ul>
			</div>
			<div class="botoesMenu">
				<a href="#" id="btn_voltar_mnu_ajuda" class="button">Voltar</a><span class="button_corner">&nbsp;</span>
				<a href="#" id="btn_fechar_mnu_ajuda" class="button">Fechar</a><span class="button_corner">&nbsp;</span>
			</div>
		</div>
	</div>
	<div id="rodape">
		<span id="lbl_criadoPor">Created by</span>: Luan Rodrigues - <a href="mailto: admin@qatool.com">admin@qatool.com</a>
		<br /><span id="lbl_versao">Version</span>: <?php echo $VERSION?>
	</div>
	</body>
</html>
