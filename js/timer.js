timer = {
	// Attributes
	seconds : 0,
	minutes : 0,
	hours : 0,
	// active means if the timer is running or not
	active : false,
	// container, where the timer will insert the string with the current time
	container: undefined,
	// attribute to maintain the timeout variable
	ticker : undefined,
	
	// Setters
	setSeconds : function(s){
		if(s > 59){
			var m = 0;
			while(s > 59){
				s -= 60;
				m++
			}
			this.seconds = s;
			this.setMinutes(this.getMinutes() + m);
		} else {
			this.seconds = s;
		}
	},
	setMinutes : function(m){
		if(m > 59){
			var h = 0;
			while(m > 59){
				m -= 60;
				h++
			}
			this.minutes = m;
			this.setHours(this.getHours() + h);
		} else {
			this.minutes = m;
		}
	},
	setHours : function(h){
		this.hours = h;
	},
	setTime : function(s,m,h){
		this.setHours(h);
		this.setMinutes(m);
		this.setSeconds(s);
	},
	setContainer : function(element){
		this.container = element;
	},
	
	// Getters
	getSeconds : function(){return this.seconds},
	getMinutes : function(){return this.minutes},
	getHours : function(){return this.hours},
	
	// getTime will always return a string, if needed values in integer, get them individually
	getTime : function(){
		var sString;
		var mString;
		
		if(this.getSeconds() < 10) sString = '0' + this.getSeconds();
		else sString = this.getSeconds();
		
		if(this.getMinutes() < 10) mString = '0' + this.getMinutes();
		else mString = this.getMinutes();
		
		return this.getHours() + ':' + mString + ':' + sString;
	},
	getContainer : function(){return this.container;},
	
	// Other functions	
	start : function(){
		if(this.container != undefined && this.active == false){
			this.active = true;
			this.ticker = setTimeout('timer.tickPrint()',1000);
		}
	},
	
	stop : function(){
		if(this.active == true){
			this.active = false;
			clearTimeout(this.ticker);
		}
	},
	
	reset : function(){
		this.stop();
		this.setTime(0,0,0);		
		this.printInContainer();
	},
	
	tickPrint : function(){
		this.setSeconds(this.getSeconds() + 1);
		this.printInContainer();
		this.ticker = setTimeout('timer.tickPrint()',1000);
	},
	
	printInContainer : function(){
		this.container.innerHTML = this.getTime();
	}
}