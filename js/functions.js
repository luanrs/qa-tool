﻿// Função para remover todas as opções, exceto a padrão, de um seletor
function limpaSeletor(seletor){
	// Pegar o elemento 'select'
	seletor = getSeletor(seletor);
	// Testar se o elemeto possui mais de uma opção, se sim, percore todas as opções maiores
	// que um e as remove
	if(seletor.options.length > 1) {
		for(j = seletor.options.length - 1; j > 0 ; j--) {
			seletor.remove(j);
		}
	}
	return seletor;
}

function getSeletor(seletor) {
	switch (seletor) {
		case 'idioma':
			seletor = document.getElementById('cmb_idioma').getElementsByTagName('select')[0];
			break;
		case 'lista':
			seletor = document.getElementById('cmb_lista').getElementsByTagName('select')[0]
			break;
		case 'delimitador':
			seletor = document.getElementById('cmb_delimitador').getElementsByTagName('select')[0]
			break;
	}
	return seletor
}

function getFilhos(elemento)
{
	var filhos = new Array;
	for (var i = 0; i < elemento.childNodes.length; i++)
	{
		if (elemento.childNodes[i].nodeType == 1){
			filhos.push(elemento.childNodes[i]);
		}
	}
	return filhos;
}

// This funcion will load the list configured
function carregarListas(modo)
{
	// Variável que conterá o objeto XMLHttpRequest que será usado para obter o XML das listas
	var conexaoLista;
	var xmlLista;
	var idiomaListaTemporario;
	var tipoListaTemporario;		
	var nomeListaTemporario;
	var listaPadraoOk = false;		
	var lista;
	var listaRepetida;
	var seletorLista;
	
	// Cria um objeto XMLHttpRequest para pegar os dados do servidor
	if (window.XMLHttpRequest) {// Código para IE7+, Firefox, Chrome, Opera, Safari
		conexaoLista = new XMLHttpRequest();
	}
	else {// Código para IE6, IE5
		conexaoLista = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	// Define um eventListener para chamar a função que continuará a carregar os dados quando o xml for carregado.
	if(!isIE){conexaoLista.addEventListener('load', continuarCargaListas, false);}else{conexaoLista.onreadystatechange = continuarCargaListas;}
	
	// Checa se os arquivos de listas definidos no arquivo de configurações (vetor 'lists') existem
	for(i = 0; i < lists.length; i++) {
		conexaoLista.open('GET','./content/lists/' + lists[i] + '/' + idiomaSelecionado + '.xml', false);
		conexaoLista.send(null);
	}
	
	// Se em nenhum momento a a lista padrão foi carregada, configura-se o valor para 0
    if (!listaPadraoOk) { listaSelecionada = '0';}
	
	
	if(modo == 'carregamentoCompleto') {
		seletorLista = limpaSeletor('lista');
		// Após testar todos as opções de idioma e criar o vetor de idiomas corrigido, pegar o seletor de idiomas e
		// adicionar as opções
		if(seletorLista.disabled == true){seletorLista.removeAttribute('disabled');}
		switch(listasCorrigido.length) {
			case 1:
				// Caso exista apenas um elemento no vetor, ele será adicionar como selecionado e o comboBox será bloqueado
				adicionarOpcaoSeletor(seletorLista, listasCorrigido[0].tipo, listasCorrigido[0].nome, true);
			case 0:
				// Caso não exista nenhum elemento no vetor, desabilitar o comboBox
				seletorLista.setAttribute('disabled', 'disabled');
				break;
			default:
				// Em qualquer outro caso, adicionar a opção no final
				for(j = 0; j < listasCorrigido.length; j++) {
					adicionarOpcaoSeletor(seletorLista, listasCorrigido[j].tipo, listasCorrigido[j].nome, false);
				}
				break;
		}
	} else {bloquearSeletor('lista',true);}
	
	function continuarCargaListas() {
		if(conexaoLista.readyState == '4' && (conexaoLista.status == '200' || conexaoLista.status == '0')) {// Só continuará se a conexão tiver se completado e com status 200 (ok)
			// Pegar o conteúdo do arquivo xml e depois carregar as infomações de idioma, tipo e nome da lista
			xmlLista = conexaoLista.responseXML;
			idiomaListaTemporario = xmlLista.getElementsByTagName('checklist')[0].getAttribute('language');
			tipoListaTemporario = xmlLista.getElementsByTagName('checklist')[0].getAttribute('type');
			nomeListaTemporario = xmlLista.getElementsByTagName('checklist')[0].getAttribute('name');			
			
			// Testa se a lista carregada é a lista padrão, caso sim, define o nome da lista selecionada por padrão
            // Caso em nenhum momento a lista padrão seja encontrada, redefine a variável para 0
			if(tipoListaTemporario.toLowerCase() == listaSelecionada.toLowerCase()){
				nomeListaSelecionada = nomeListaTemporario;
				listaPadraoOk = true;
			}
			
			// Agora irá criar um objeto lista com as informações carregadas
			lista = new Lista(idiomaListaTemporario,tipoListaTemporario,nomeListaTemporario);
			
			// Após criar o objeto lista, irá adicioná-lo no vetor com as listas corrigidos
			// Primeiro irá checar se o vetor está vazio, caso sim, irá adicionar o objeto diretamente
			if(listasCorrigido.length == 0) {
				listasCorrigido.push(lista);
			} else { // Caso contrário irá vasculhar o vetor para verificar se o tipo já existe
				listaRepetida = false;
				for (var j = 0; j < listasCorrigido.length; j++) {
					// Caso exista, sairá do laço
					if(listasCorrigido[j].tipo.toLowerCase() == lista.tipo.toLowerCase()){listaRepetida = true; break;}
				}
				
				//Se não encontrar, irá adicionar ao vetor de listas corrigido
				if(!listaRepetida){listasCorrigido.push(lista)}
			}
		}
	}
}

// This funcion will load the list of delimiters
function carregarDelimitadores(modo)
{
	// Variável que conterá o objeto XMLHttpRequest que será usado para obter o XML com os idiomas
	var conexaoDelimitadores;
	var xmlDelimitadores;
	var delimitadores;
	var codigoDelimitadorTemporario;
	var nomeDelimitadorTemporario;
	var delimitadorPadraoOk = false;
	var delimitador;
	var delimitadorRepetido;
	
	// Limpa o seletor de listas e depoir carrega ele para ser usado posteriormente	
	var seletorDelimitador;
	
	// Cria um objeto XMLHttpRequest para pegar os dados do servidor
	if (window.XMLHttpRequest) {// Código para IE7+, Firefox, Chrome, Opera, Safari
		conexaoDelimitadores = new XMLHttpRequest();
	}
	else {// Código para IE6, IE5
		conexaoDelimitadores = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	// Define um eventListener para chamar a função que continuará a carregar os dados quando o xml for carregado.
	if(!isIE){conexaoDelimitadores.addEventListener('load',continuarCargaDelimitadores,false);}else{onexaoDelimitadores.onreadystatechange = continuarCargaDelimitadores;}
	
	// Checa se o arquivo de delimitadores existe para o idioma selecionado
	conexaoDelimitadores.open('GET','./content/lists/delimiters/' + idiomaSelecionado + '.xml', false);
	conexaoDelimitadores.send(null);
	
	// Se em nenhum momento o delimitador padrão foi carregado, configura-se o valor para 0
    if (!delimitadorPadraoOk) { delimitadorSelecionado = '0' }
	
	if(modo == 'carregamentoCompleto'){
		seletorDelimitador = limpaSeletor('delimitador');
		// Após testar todos as opções de idioma e criar o vetor de idiomas corrigido, pegar o seletor de idiomas e
		// adicionar as opções
		if(seletorDelimitador.disabled == true){seletorDelimitador.removeAttribute('disabled');}
		switch(delimitadoresCorrigido.length) {
			case 1:
				// Caso exista apenas um elemento no vetor, ele será adicionar como selecionado e o comboBox será bloqueado
				adicionarOpcaoSeletor(seletorDelimitador, delimitadoresCorrigido[0].codigo, delimitadoresCorrigido[0].nome, true);
			case 0:
				// Caso não exista nenhum elemento no vetor, desabilitar o comboBox
				seletorDelimitador.setAttribute('disabled', 'disabled');
				break;
			default:
				// Em qualquer outro caso, adicionar a opção no final
				for(var j = 0; j < delimitadoresCorrigido.length; j++) {
					adicionarOpcaoSeletor(seletorDelimitador, delimitadoresCorrigido[j].codigo, delimitadoresCorrigido[j].nome, false);
				}
				break;
		}
	} else {bloquearSeletor('delimitador',true);}
}

// This funcion will load the list of delimiters
function carregarDelimitadores(modo)
{
	// Variável que conterá o objeto XMLHttpRequest que será usado para obter o XML com os idiomas
	var conexaoDelimitadores;
	var xmlDelimitadores;
	var delimitadores;
	var codigoDelimitadorTemporario;
	var nomeDelimitadorTemporario;
	var delimitadorPadraoOk = false;
	var delimitador;
	var delimitadorRepetido;
	var seletorDelimitador;
	
	// Cria um objeto XMLHttpRequest para pegar os dados do servidor
	if (window.XMLHttpRequest) {// Código para IE7+, Firefox, Chrome, Opera, Safari
		conexaoDelimitadores = new XMLHttpRequest();
	}
	else {// Código para IE6, IE5
		conexaoDelimitadores = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	// Define um eventListener para chamar a função que continuará a carregar os dados quando o xml for carregado.
	if(!isIE){conexaoDelimitadores.addEventListener('load',continuarCargaDelimitadores,false);}else{conexaoDelimitadores.onreadystatechange = continuarCargaDelimitadores;}
	
	// Checa se o arquivo de delimitadores existe para o idioma selecionado
	conexaoDelimitadores.open('GET','./content/lists/delimiters/' + idiomaSelecionado + '.xml', false);
	conexaoDelimitadores.send(null);
	
	// Se em nenhum momento o delimitador padrão foi carregado, configura-se o valor para 0
    if (!delimitadorPadraoOk) { delimitadorSelecionado = '0' }
	
	
	if(modo == 'carregamentoCompleto'){
		seletorDelimitador = limpaSeletor('delimitador');
		// Após testar todos as opções de idioma e criar o vetor de idiomas corrigido, pegar o seletor de idiomas e
		// adicionar as opções
		if(seletorDelimitador.disabled == true){seletorDelimitador.removeAttribute('disabled');}
		switch(delimitadoresCorrigido.length) {
			case 1:
				// Caso exista apenas um elemento no vetor, ele será adicionar como selecionado e o comboBox será bloqueado
				adicionarOpcaoSeletor(seletorDelimitador, delimitadoresCorrigido[0].codigo, delimitadoresCorrigido[0].nome, true);
			case 0:
				// Caso não exista nenhum elemento no vetor, desabilitar o comboBox
				seletorDelimitador.setAttribute('disabled', 'disabled');
				break;
			default:
				// Em qualquer outro caso, adicionar a opção no final
				for(var j = 0; j < delimitadoresCorrigido.length; j++) {
					adicionarOpcaoSeletor(seletorDelimitador, delimitadoresCorrigido[j].codigo, delimitadoresCorrigido[j].nome, false);
				}
				break;
		}
	} else {bloquearSeletor('delimitador',true);}
	
	function continuarCargaDelimitadores() {
		if(conexaoDelimitadores.readyState == '4' && (conexaoDelimitadores.status == '200' || conexaoDelimitadores.status == '0')) {// Só continuará se a conexão tiver se completado e com status 200 (ok)
			// Pegar o conteúdo do arquivo xml e depois carregar as infomações de idioma, tipo e nome da lista
			xmlDelimitadores = conexaoDelimitadores.responseXML;
			delimitadores = xmlDelimitadores.getElementsByTagName('delimiter');			
			
			for(i = 0; i < delimitadores.length; i++) {
				codigoDelimitadorTemporario = delimitadores[i].getAttribute('code');
				nomeDelimitadorTemporario = delimitadores[i].getAttribute('name');
				
				// Testa se o delimitador carregado é o delimitador padrão, caso sim, define o nome do delimitador selecionado por padrão
				// Caso em nenhum momento o delimitador padrão seja encontrado, redefine a variável para 0
				if(codigoDelimitadorTemporario.toLowerCase() == delimitadorSelecionado.toLowerCase()){
					nomeDelimitadorSelecionado = nomeDelimitadorTemporario;
					delimitadorPadraoOk = true;
				}
				
				// Agora irá criar um objeto delimitador com as informações carregadas
				delimitador = new Delimitador(codigoDelimitadorTemporario, nomeDelimitadorTemporario);			
			
				// Após criar o objeto delimitador, irá adicioná-lo no vetor com os delimitadores corrigido
				// Primeiro irá checar se o vetor está vazio, caso sim, irá adicionar o objeto diretamente
				if(delimitadoresCorrigido.length == 0) {
					delimitadoresCorrigido.push(delimitador);
				} else { // Caso contrário irá vasculhar o vetor para verificar se o tipo já existe
					for (var j= 0; j < delimitadoresCorrigido.length; j++) { // Caso contrário irá vasculhar o vetor para verificar se o tipo já existe
						if(delimitadoresCorrigido[j].codigo.toLowerCase() == delimitador.codigo.toLowerCase()) {
						delimitadorRepetido = true; break;}
					}
					
					if(!delimitadorRepetido) {delimitadoresCorrigido.push(delimitador);}
				}				
			}
		}
	}
}

function atualizarDescConfig(){
	var descricao;
	
	descricao = document.getElementById('lbl_idiomaSelecionadoValor');
	descricao.innerHTML = descricao.innerHTML.substring(0,descricao.innerHTML.search(':') + 1);
	descricao.innerHTML += ' ' + nomeIdiomaSelecionado;	
		
	descricao = document.getElementById('lbl_listaSelecionadaValor');
	descricao.innerHTML = descricao.innerHTML.substring(0,descricao.innerHTML.search(':') + 1);
	descricao.innerHTML += ' ' + nomeListaSelecionada;	
	
	descricao = document.getElementById('lbl_delimitadorSelecionadoValor');
	descricao.innerHTML = descricao.innerHTML.substring(0,descricao.innerHTML.search(':') + 1);
	descricao.innerHTML += ' ' + nomeDelimitadorSelecionado;	
}

function carregarConteudo() {
	var conexaoConteudo;
	var xmlConteudo;
	var secoesXML;
	
	// Cria um objeto XMLHttpRequest para pegar os dados do servidor
	if (window.XMLHttpRequest) {// Código para IE7+, Firefox, Chrome, Opera, Safari
		conexaoConteudo=new XMLHttpRequest();
	}
	else {// Código para IE6, IE5
		conexaoConteudo=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	if(!isIE){conexaoConteudo.addEventListener('load',continuarCargaConteudo,false);}else{conexaoConteudo.onreadystatechange = continuarCargaConteudo}
	conexaoConteudo.open('GET','./content/lists/' + listaSelecionada + '/' + idiomaSelecionado +'.xml', false);
	conexaoConteudo.send(null);	
	
	function continuarCargaConteudo(){
		if(conexaoConteudo.readyState == '4' && (conexaoConteudo.status == '200' || conexaoConteudo.status == '0')) {
			xmlConteudo = conexaoConteudo.responseXML;
			secoesXML = xmlConteudo.getElementsByTagName('section');
			for(var i = 0; i < secoesXML.length; i++) {
				if(secoesXML[i].parentNode.nodeName == 'checklist'){
					carregarSecao(secoesXML[i]);
				}
			}
		}
	}
}

//This Will Check if the option is for the selected delimiter
function paraNaoPara(elemento) {
	if(delimitadorSelecionado != '0'){	
		var atributoPara;
		var atributoNaoPara;
		
		atributoPara = elemento.getAttribute('for');
		if(atributoPara != null && atributoPara != undefined && atributoPara != ''){
			atributoPara = atributoPara.toLowerCase();
			atributoPara = (atributoPara.search(delimitadorSelecionado.toLowerCase())==-1)?false:true;
		} else {atributoPara = true}
		
		atributoNaoPara = elemento.getAttribute('notFor');
		if(atributoNaoPara != null && atributoNaoPara != undefined && atributoNaoPara != ''){
			atributoNaoPara = atributoNaoPara.toLowerCase();
			atributoNaoPara = (atributoNaoPara.search(delimitadorSelecionado.toLowerCase())==-1)?false:true;
		} else {atributoNaoPara = false}
		
		if(atributoPara != false && atributoNaoPara == false){return true}
		else {return false;}
	} else {return true;}
}

function carregarSecao(elemento){
	var paraEsseDelimitador;
	var idSecao;
	var nomeSecao;
	var secaoVisivel;
	var secao;
	var filhos;
	var idOpcao;
	var nomeOpcao;
	var dicaOpcao;
	var opcao;
	var subsecao;

	paraEsseDelimitador = paraNaoPara(elemento);
	if(paraEsseDelimitador){
		idSecao = 'sec' + secoes.length;		
		nomeSecao = elemento.getAttribute('name');
		secaoVisivel = elemento.getAttribute('defaultShow') == 'show'?true:false;
		secao = new Secao(idSecao,nomeSecao,secaoVisivel,elemento.parentNode.nodeName == 'checklist'?null:elemento.parentNode);
		secoes.push(secao);
		filhos = getFilhos(elemento);
		for (var j = 0; j < filhos.length; j++) {
			switch (filhos[j].nodeName) {
				case 'option':
					paraEsseDelimitador = paraNaoPara(filhos[j]);
					if(paraEsseDelimitador){
						idOpcao = 'opt' + opcoes.length;
						nomeOpcao = filhos[j].getAttribute('name');
						dicaOpcao = carregarDica(filhos[j]);
						opcao = new Opcao(idOpcao,nomeOpcao,dicaOpcao,false,secao);
						opcao.atualizarStatus();
						secao.adicionarOpcao(opcao);
						opcoes.push(opcao)
					}
					break;
				case 'section':
					paraEsseDelimitador = paraNaoPara(filhos[j]);
					if(paraEsseDelimitador){
						subsecao = carregarSecao(filhos[j]);
						secao.adicionarOpcao(subsecao);
					}
					break;
				default: continue;
			}
		}
	}
	return secao;
}

function mudarIdioma(e) {
	idiomaSelecionado = e.target.value;
	if(idiomaSelecionado == '0'){
		desativarBotao(document.getElementById('btn_mudar'));
		bloquearSeletor('lista',true);
		bloquearSeletor('delimitador',true);
	} else {
		listasCorrigido = [];
		delimitadoresCorrigido = [];
		carregarListas('carregamentoCompleto');
		carregarDelimitadores('carregamentoCompleto');
	}
}

function mudarLista(e) {
	if(e.target.value != '0'){
		ativarBotao(document.getElementById('btn_mudar'));
	} else {
		desativarBotao(document.getElementById('btn_mudar'));
	}
}

// Essa função irá criar os elementos de opcão e irá adicionar-los nos seletores
function adicionarOpcaoSeletor(seletor, valor, texto, selecionado) {
	var opcao = document.createElement('option');
	opcao.value = valor;
	opcao.text = texto;
	if (selecionado) {opcao.selected = 'true';}
	if(!isIE){seletor.add(opcao,null);}else{seletor.add(opcao,0);}
}

function bloquearSeletor(seletor, apagar) {
	if(apagar){seletor = limpaSeletor(seletor);} else {seletor = getSeletor(seletor);}
	seletor.setAttribute('disabled','disabled');
}

function escreverConteudo() {	
	var conteudo = document.getElementById('conteudo');
	for(var i = 0; i < secoes.length; i++) {
		if(!secoes[i].getMae())
		escreverSecao(secoes[i], conteudo);
	}
	
	for(var i = 0; i < opcoes.length; i++) {
		if(!isIE){document.getElementById('opt' + i).addEventListener('click', cliqueOpcao, false);}else{document.getElementById('opt' + i).onclick = cliqueOpcao;}
		if(!isIE){document.getElementById('opt' + i + '_rotulo').addEventListener('click', cliqueOpcao, false);	}else{document.getElementById('opt' + i + '_rotulo').onclick = cliqueOpcao;}
		if(document.getElementById('opt' + i + '_dica_rotulo')){
			if(!isIE){document.getElementById('opt' + i + '_dica_rotulo').addEventListener('click', function(e){expandirDicaComentario(e.target,'dica');}, false);}
			else{document.getElementById('opt' + i + '_dica_rotulo').onclick = function(e){expandirDicaComentario(e.target,'dica');}}
		}
		if(!isIE){document.getElementById('opt' + i + '_comentario_rotulo').addEventListener('click', function(e){expandirDicaComentario(e.target,'comentario');}, false);	}else{document.getElementById('opt' + i + '_comentario_rotulo').onclick = function(e){expandirDicaComentario(e.target,'comentario');}}
		if(!isIE){document.getElementById('opt' + i + '_comentario').addEventListener('change', adicionarComentario, false);}else{document.getElementById('opt' + i + '_comentario').onchange = adicionarComentario;}
	}
	
	for(var i = 0; i < secoes.length; i++) {
		if(!isIE){document.getElementById('sec' + i).addEventListener('click', cliqueSecao, false);}else{document.getElementById('sec' + i).onclick = cliqueSecao;}
	}
	
	if(checarAbertos('dica') && !checarOcultos('dica')){expandidoDicas = true;} else {expandidoDicas = false;}
	if(checarAbertos('comentario') && !checarOcultos('comentario')){expandidoDicas = true;} else {expandidoDicas = false;}
	if(checarAbertos('secao') && !checarOcultos('secao')){expandidoSecoes = true;} else {expandidoSecoes = false;}
	
	atualizarMenuSuperior('dica');
	atualizarMenuSuperior('comentario');
	atualizarMenuSuperior('secao');
}

function escreverSecao(secao, elementoPai) {
	var conteinerSecao;
	var tituloSecao;
	var opcoesSecao;
	
	conteinerSecao = document.createElement('div');
	
	tituloSecao = document.createElement('span');
	tituloSecao.setAttribute('id', secao.id)
	tituloSecao.setAttribute('class','tituloSecao')
	tituloSecao.appendChild(document.createTextNode(secao.nome))
	
	opcoesSecao = document.createElement('div');
	opcoesSecao.setAttribute('id', secao.id + '_conteudo');
	opcoesSecao.setAttribute('class', 'conteudoSecao');
	if(secao.getVisivel()){
		opcoesSecao.style.display = 'block';
	} else {
		opcoesSecao.style.display = 'none';
	}
	
	for(var i = 0; i < secao.opcoes.length; i++){
		switch (secao.opcoes[i].id.substring(0,3)){
			case 'opt':
				escreverOpcao(secao.opcoes[i], opcoesSecao);
				break;
			case 'sec':
				escreverSecao(secao.opcoes[i], opcoesSecao);
				break;
		}
		
	}
	
	conteinerSecao.appendChild(tituloSecao);
	conteinerSecao.appendChild(opcoesSecao);	
	elementoPai.appendChild(conteinerSecao);
}

function escreverOpcao(opcao, secaoMae) {
	var spanBloco;
	var spanOpcao;
	var labelOpcao;
	var spanDica;
	var spanCome;
	var dicas;
	var divDica;
	var divComentario;
	var comentario;
	var spanExibeDica;
	var spanExibeComentario;
	
	spanBloco = document.createElement('span');
	
	spanOpcao = document.createElement('span');
	spanOpcao.setAttribute('class','opcao');
	spanOpcao.setAttribute('id',opcao.id);
	
	labelOpcao = document.createElement('span');
	labelOpcao.appendChild(document.createTextNode(opcao.nome));
	labelOpcao.setAttribute('id',opcao.id + '_rotulo');
	labelOpcao.setAttribute('class','rotuloOpcao');
	
	spanDica = document.createElement('span');
	spanDica.appendChild(document.createTextNode(HINT));
	spanDica.setAttribute('id',opcao.id + '_dica_rotulo');
	spanDica.setAttribute('class','smallLink');
	
	spanCome = document.createElement('span');
	spanCome.appendChild(document.createTextNode(COMMENT));
	spanCome.setAttribute('id',opcao.id + '_comentario_rotulo');
	spanCome.setAttribute('class','smallLink');
	
	divDica = document.createElement('div');		
	divDica.setAttribute('class','dica');
	divDica.setAttribute('id',opcao.id + '_dica');

	divDica.innerHTML = opcao.getDica();
	
	divComentario = document.createElement('div');
	divComentario.setAttribute('class','comentario');
	divComentario.setAttribute('id',opcao.id + '_comentario');
	
	comentario = document.createElement('textarea');

	divComentario.appendChild(comentario);
	
	spanBloco.appendChild(spanOpcao);
	spanBloco.appendChild(document.createTextNode(' '));
	spanBloco.appendChild(labelOpcao);
	spanBloco.appendChild(document.createTextNode(' '));
	if(trim(opcao.getDica()) != ''){spanBloco.appendChild(spanDica);spanBloco.appendChild(document.createTextNode(' - '));}
	spanBloco.appendChild(document.createTextNode(' '));
	spanBloco.appendChild(spanCome);	
	spanBloco.appendChild(document.createElement('br'));
	if(trim(opcao.getDica()) != ''){spanBloco.appendChild(divDica);}
	spanBloco.appendChild(divComentario);
	
	secaoMae.appendChild(spanBloco);
}

function carregarDica(opcao){
	var delimitadores;
	var delimitador;
	var dicaFinal = '';
	var para = '';
	var naoPara = '';
	var dicas = opcao.getElementsByTagName('hint');
	if(delimitadorSelecionado == '' || delimitadorSelecionado == '0' || delimitadorSelecionado == null){
		for(var i = 0; i < dicas.length; i++) {
			para = '';
			if(delimitadores = dicas[i].getAttribute('for')){
				delimitadores = delimitadores.split(',');
				for(var j = 0; j < delimitadores.length; j++){
					delimitador = procuraDelimitador(trim(delimitadores[j]));
					if(para == ''){para += FOR_TEXT + ': ' + delimitador;}
					else {para += ', ' + delimitador;}
				}
			}
			if(delimitadores = dicas[i].getAttribute('notFor')){
				delimitadores = delimitadores.split(',');
				for(var j = 0; j < delimitadores.length; j++){
					delimitador = procuraDelimitador(trim(delimitadores[j]));
					if(para == ''){para += NOT_FOR_TEXT + ': ' + delimitador;}
					else {para += ', ' + delimitador;}
				}
			}
			dicaFinal += para;
			if(trim(dicas[i].textContent) != ''){dicaFinal += '<p>' + trim(dicas[i].textContent) + '</p>';}
		}
	} else {
		for(var i = 0; i < dicas.length; i++) {
			paraEsseDelimitador = paraNaoPara(dicas[i]);
			if(paraEsseDelimitador){
				if(trim(dicas[i].textContent) != ''){dicaFinal += '<p>' + trim(dicas[i].textContent) + '</p>';}
			}
		}
	}
	return dicaFinal;
}

function recarregarConteudo() {
	var seletor;
	
	seletor = getSeletor('idioma');
	idiomaSelecionado = seletor.value;
	nomeIdiomaSelecionado = seletor.options[seletor.selectedIndex].text;
	
	seletor = getSeletor('lista');
	listaSelecionada = seletor.value;
	nomelListaSelecionada = seletor.options[seletor.selectedIndex].text;
	
	seletor = getSeletor('delimitador');
	delimitadorSelecionado = seletor.value;
	if(delimitadorSelecionado != '0'){
		nomeDelimitadorSelecionado = seletor.options[seletor.selectedIndex].text;
	} else {
		nomeDelimitadorSelecionado = ''
	}
	
	porcentagemCompleta = new Porcentagem(0,true);
	
	atualizarDescConfig();	
	
	carregarLocalizacoes();
	
	apagarConteudo()
	carregarConteudo();
	escreverConteudo();
	
	definirTitulo();
	recriarMenus();
}

function desativarBotao(botao){
	botao.setAttribute('disabled','disabled')
}

function ativarBotao(botao){
	botao.removeAttribute('disabled')
}

function apagarConteudo() {
	var conteudo = document.getElementById('conteudo');
	var seletorIdiomas = getSeletor('idioma');
	while(conteudo.childNodes.length != 0){
		conteudo.removeChild(conteudo.childNodes[0])
	}
	secoes = [];
	opcoes = [];
	
	if(seletorIdiomas.options.length > 2){
		bloquearSeletor('lista',true);
		bloquearSeletor('delimitador',true);
		getSeletor('idioma').selectedIndex = 0;
	}
}

function  definirTitulo(){
	document.title = TITLE + ' - ' + porcentagemCompleta.getPorcentagem() + ' ' + TITLE_DONE;
}

function cliqueOpcao(e) {
	var id;
	if(e.target.id.search('rotulo') != -1){
		id = e.target.id.substring(3,this.id.search('_'));
	} else {
		id = e.target.id.substring(3);
	}
	if(!e.ctrlKey && !e.altKey && !e.shiftKey) {
		if(opcoes[id].getCerto()){
			opcoes[id].setCerto(false);
			mudaStatus(opcoes[id]);
		} else {
			opcoes[id].setCerto(true);
			mudaStatus(opcoes[id]);
		}
		atualizaPorcentagemCompleta();
	} else {
		if(e.ctrlKey && ! e.shiftKey){
			expandirDicaComentario(id,'dica');
			e.preventDefault();
		}else if(e.shiftKey && !e.ctrlKey){
			expandirDicaComentario(id,'comentario');
			e.preventDefault();
		} else {
			expandirDicaComentario(id,'dica');
			expandirDicaComentario(id,'comentario');
			e.preventDefault();
		}
	}
}

function atualizaPorcentagemCompleta(){
	var opcoesMarcadas = contaOpcoesMarcadas();
	porcentagemCompleta.setValor(opcoesMarcadas/opcoes.length);
	definirTitulo();
}

function contaOpcoesMarcadas(){
	var opcoesMarcadas = 0;
	for(var i = 0; i < opcoes.length; i++){
		if(opcoes[i].certo == true){opcoesMarcadas++}
	}
	return opcoesMarcadas;
}

function getNextSinbling(elemento){
	proximoElemento = elemento.nextSibling;
	while(proximoElemento != null){
		if(proximoElemento.nodeType == 1){
			return proximoElemento;
		} else {
			proximoElemento = proximoElemento.nextSibling;
		}
	}
	return null;
}

function cliqueExibeDicaComentario(e){
	var tipo = this.getAttribute('class').substring(0,4);
	var id = this.getAttribute('id').substring(10);
	var bloco = document.getElementById(tipo + '_' + id);
	if(bloco.style.display == 'none'){
		bloco.style.display = 'block';
	} else {
		bloco.style.display = 'none';
	}
}

function cliqueSecao(e) {
	var secao = e.target.id.replace(/sec/,'');
	secoes[secao].trocaVisibilidade();
	if(checarAbertos('secao') && !checarOcultos('secao')){expandidoSecoes = true;}else{expandidoSecoes = false;}
	atualizarMenuSuperior('secao');
}

function combinacaoTeclado(e)
{
	// Letra M
	if(e.ctrlKey && e.keyCode == 77 && !e.altKey){
		abreMenuPrincipal();
		e.preventDefault();
	}
	// Letra R
	if(e.ctrlKey && e.keyCode == 82 && !e.altKey){
		var temAberto;
		carregarRelatorio();
		for(var i = 0; i < menus.length; i++) {
			if (menus[i].getId() == 'mnu_relatorio' && menus[i].getVisivel() == true){
				temAberto = true;
				break;
			}
		}
		if(temAberto){
			fecharTodosMenus();
		} else {
			abrirMenu('mnu_relatorio',true,false);
		}
		e.preventDefault();
	}
	// Letra D
	if(e.ctrlKey && e.keyCode == 68 && !e.altKey){
		expandirDicasComentarios('dica',true);
		e.preventDefault();
	}
	// Letra A
	if(e.ctrlKey && e.keyCode == 65 && !e.shiftKey){
		expandirDicasComentarios('comentario',true);
		e.preventDefault();
	}
	// Letra E
	if(e.ctrlKey && e.keyCode == 69 && !e.altKey){
		expandirSecoes(true);
		e.preventDefault();
	}
	// ESC
	if(e.keyCode == 27){
		fecharTodosMenus();
		e.preventDefault();
	}
	// Ctrl Alt A
	if(e.shiftKey && e.ctrlKey && e.keyCode == 65){
		var temAberto;
		for(var i = 0; i < menus.length; i++) {
			if (menus[i].getId() == 'mnu_ajuda' && menus[i].getVisivel() == true){
				temAberto = true;
				break;
			}
		}
		if(temAberto){
			fecharTodosMenus();
		} else {
			abrirMenu('mnu_ajuda',true,false);
		}
		e.preventDefault();
	}
}

function abrirMenu(id,btnFechar,btnVoltar){
	var posicao = procurarPosicaoMenu(id);
	var fundoNegro = document.getElementById('fundoNegro');
	if(fundoNegro.style.display != 'block'){fundoNegro.style.display = 'block';}
	
	if(!menus[posicao].getVisivel()){
		menus[posicao].exibirMenu();
		for(var i = 0; i < menus.length; i++){
			if(i != posicao){
				menus[i].esconderMenu();
			}
		}
		if(menus[posicao].id != 'mnu_principal' && btnFechar){menus[posicao].fecharVisivel();} else if(menus[posicao].id != 'mnu_principal') {menus[posicao].fecharInvisivel();}
		if(menus[posicao].id != 'mnu_principal' && btnVoltar){menus[posicao].voltarVisivel();} else if(menus[posicao].id != 'mnu_principal') {menus[posicao].voltarInvisivel();}
	}
}

function mudaStatus(opcao){
	var elemento = document.getElementById(opcao.getId());
	switch(opcao.getStatus()){
		default: elemento.style.backgroundPosition = '-13px 0'; break;
		case 1: elemento.style.backgroundPosition = '0 0'; break;
		case 2: elemento.style.backgroundPosition = '-39px 0'; break;
		case 3: elemento.style.backgroundPosition = '-26px 0'; break;
	}
}

function fecharTodosMenus(){
	var fundoNegro = document.getElementById('fundoNegro');
	var menuTopo = document.getElementById('mnu_topo');
	var menuBase = document.getElementById('mnu_base');
		
	for(var i = 0; i < menus.length; i++){
		menus[i].esconderMenu();
	}
	fundoNegro.style.display = 'none';
	
	menuTopo.style.display = 'none';
	menuBase.style.display = 'none';
}

function expandirDicasComentarios(tipo,expandir){
	var temAberto = false;
	var elemento;
	switch(tipo){
		case 'dica':
			if(expandidoDicas){
				for(i = 0; i < opcoes.length; i++){
					opcoes[i].esconderDica();
				}
				expandidoDicas = false;
			} else {
				temAberto = checarAbertos(tipo);
				if(temAberto && !expandir){
					for(i = 0; i < opcoes.length; i++){
						opcoes[i].esconderDica();
					}
					expandidoDicas = false;
				} else {
					for(i = 0; i < opcoes.length; i++){
						opcoes[i].mostrarDica();
					}
					expandidoDicas = true;
				}
			}
			atualizarMenuSuperior(tipo);
			break;
		case 'comentario':
			if(expandidoComentarios){
				for(i = 0; i < opcoes.length; i++){
					opcoes[i].esconderComentario();
				}
				expandidoComentarios = false;
			} else {
				temAberto = checarAbertos(tipo);
				if(temAberto && !expandir){
					for(i = 0; i < opcoes.length; i++){
						opcoes[i].esconderComentario();
					}
					expandidoComentarios = false;
				} else {
					for(i = 0; i < opcoes.length; i++){
						opcoes[i].mostrarComentario();
					}
					expandidoComentarios = true;
				}
			}
			atualizarMenuSuperior(tipo);
			break;
	}
}

function expandirSecoes(expandir){
	var temAberto;
	if(expandidoSecoes){
		for(i = 0; i < secoes.length; i++){
			secoes[i].esconderSecao();			
		}
		expandidoSecoes = false;
	} else {
		temAberto = checarAbertos('secao');
		if(temAberto && !expandir){
			for(i = 0; i < secoes.length; i++){
				secoes[i].esconderSecao();				
			}
			expandidoSecoes = false;
		} else {
			for(i = 0; i < secoes.length; i++){
				secoes[i].mostrarSecao();				
			}
			expandidoSecoes = true;
		}
	}
	atualizarMenuSuperior('secao');
}

function adicionarComentario(e){
	var opcao = this.id.replace(/(opt)|(_comentario)/g,'');
	opcoes[opcao].setComentario(e.target.value);
	mudaStatus(opcoes[opcao]);
}

function trim(str){
	if(str != undefined || str != null)return str.replace(/^\s+|\s+$/g,"");
}

function checarAbertos(tipo){
	var temAberto = false;
	switch(tipo){
		case 'dica':
			for(var i = 0; i < opcoes.length; i++){
				if(opcoes[i].getDicaVisivel()){
					temAberto = true;
					break;
				}
			}
			break;
		case 'comentario':
			for(var i = 0; i < opcoes.length; i++){
				if(opcoes[i].getComentarioVisivel()){
					temAberto = true;
					break;
				}
			}
			break;
		case 'secao':
		for(var i = 0; i < secoes.length; i++){
			if(secoes[i].getVisivel()){
				temAberto = true;
				break;
			}
		}
		break;
	}
	return temAberto;
}

function checarOcultos(tipo){
	var temOculto = false;
	switch(tipo){
		case 'dica':
			for(var i = 0; i < opcoes.length; i++){
				if(!opcoes[i].getDicaVisivel()){
					temOculto = true;
					break;
				}
			}
			break;
		case 'comentario':
			for(var i = 0; i < opcoes.length; i++){
				if(!opcoes[i].getComentarioVisivel()){
					temOculto = true;
					break;
				}
			}
			break;
		case 'secao':
		for(var i = 0; i < secoes.length; i++){
			if(!secoes[i].getVisivel()){
				temOculto = true;
				break;
			}
		}
		break;
	}
	return temOculto;
}

function fecharMenu(e){
	var menu = getMenuPai(e.target);
	var fundoNegro = document.getElementById('fundoNegro');
	var menuTopo = document.getElementById('mnu_topo');
	var menuBase = document.getElementById('mnu_base');
	
	if(menu == 'mnu_principal'){
		fundoNegro.style.display = 'none';
		menuTopo.style.display = 'none';
		menuBase.style.display = 'none';
	} else {
		menus[0].exibirMenu();
	}
	if(menu != null){
		menus[procurarPosicaoMenu(menu)].esconderMenu();
	}
}

function procurarPosicaoMenu(id){
	for(var i = 0; i < menus.length; i++){
		if(menus[i].getId() == id){
			return i;
		}
	}
	return NaN;
}

function getMenuPai(objeto){
	var menu = null;
	var noPai;
	var noFilho = objeto;
	do {
		noPai = noFilho.parentNode;
		if(noPai.nodeName.toLowerCase() == 'div' && noPai.id && noPai.id.search(/mnu/) != -1){
			menu = noFilho.parentNode;
		} else {
			noFilho = noPai;
		}
	}while(menu == null && noPai.nodeName.toLowerCase != 'html')
	
	if(noPai.nodeName.toLowerCase != 'html'){
		return noPai.id;
	} else {
		return null;
	}
}

function expandirDicaComentario(id,tipo){
	switch(tipo){
		case 'dica':
			id = id.id?id.id.replace(/(_dica_rotulo)|(opt)/g,''):id;
			if(opcoes[id].getDicaVisivel()){
				opcoes[id].esconderDica();
			} else {
				opcoes[id].mostrarDica();
			}
			if(checarAbertos(tipo) && !checarOcultos(tipo)){expandidoDicas = true;} else {expandidoDicas = false;}
			break;
		case 'comentario':
			id = id.id?id.id.replace(/(_comentario_rotulo)|(opt)/g,''):id;
			if(opcoes[id].getComentarioVisivel()){
				opcoes[id].esconderComentario();
			} else {
				opcoes[id].mostrarComentario();
			}
			break;
			if(checarAbertos(tipo) && !checarOcultos(tipo)){expandidoComentarios = true;} else {expandidoComentarios = false;}
	}
	atualizarMenuSuperior(tipo);
}

function atualizarMenuSuperior(tipo){
	temAberto = checarAbertos(tipo);
	switch(tipo){
		case 'dica':
			if(expandidoDicas){
				document.getElementById('btn_dicasExibir').style.display = 'none';
				document.getElementById('btn_dicasEsconder').style.display = 'inline';
				document.getElementById('dmr_exibirDicas').style.display = 'none';
			} else {
				if(temAberto){
					document.getElementById('btn_dicasExibir').style.display = 'inline';
					document.getElementById('btn_dicasEsconder').style.display = 'inline';
					document.getElementById('dmr_exibirDicas').style.display = 'inline';
				} else {
					document.getElementById('btn_dicasExibir').style.display = 'inline';
					document.getElementById('btn_dicasEsconder').style.display = 'none';
					document.getElementById('dmr_exibirDicas').style.display = 'none';
				}
			}
			break;
		case 'comentario':
			if(expandidoComentarios){
				document.getElementById('btn_comentariosExibir').style.display = 'none';
				document.getElementById('btn_comentariosEsconder').style.display = 'inline';
				document.getElementById('dmr_exibirComentarios').style.display = 'none';
			} else {
				if(temAberto){
					document.getElementById('btn_comentariosExibir').style.display = 'inline';
					document.getElementById('btn_comentariosEsconder').style.display = 'inline';
					document.getElementById('dmr_exibirComentarios').style.display = 'inline';
				} else {
					document.getElementById('btn_comentariosExibir').style.display = 'inline';
					document.getElementById('btn_comentariosEsconder').style.display = 'none';
					document.getElementById('dmr_exibirComentarios').style.display = 'none';
				}
			}
			break;
		case 'secao':
			if(expandidoSecoes){
				document.getElementById('btn_secoesExibir').style.display = 'none';
				document.getElementById('btn_secoesEsconder').style.display = 'inline';
				document.getElementById('dmr_exibirSecoes').style.display = 'none';
			} else {
				if(temAberto){
					document.getElementById('btn_secoesExibir').style.display = 'inline';
					document.getElementById('btn_secoesEsconder').style.display = 'inline';
					document.getElementById('dmr_exibirSecoes').style.display = 'inline';
				} else {
					document.getElementById('btn_secoesExibir').style.display = 'inline';
					document.getElementById('btn_secoesEsconder').style.display = 'none';
					document.getElementById('dmr_exibirSecoes').style.display = 'none';
				}
			}
			break;
	}
}

function procuraDelimitador(valor){
	for(var i = 0;i < delimitadoresCorrigido.length; i++){
		if(delimitadoresCorrigido[i].getCodigo().toLowerCase() == valor.toLowerCase()){
			return delimitadoresCorrigido[i].getNome();
		}
	}
	return null;
}

function carregarRelatorio(){
	var opcoesErradas = new Array();
	var paragrafo;
	var titulo;
	var conteudoRelatorio = document.getElementById('conteudo_relatorio');
	
	// carregarOpcoes erradas
	for(var i = 0; i < opcoes.length; i++){
		if(opcoes[i].getCerto() == false || opcoes[i].getComentado() == true){
			opcoesErradas.push(opcoes[i]);
		}
	}
	
	conteudoRelatorio.innerHTML = '';
	
	// Escreve dados
	for(var i = 0; i < opcoesErradas.length; i++){
		titulo = document.createElement('span');
		titulo.appendChild(document.createTextNode(opcoesErradas[i].getSecao().getNome() + ' - ' + opcoesErradas[i].getNome()));
		if(!opcoesErradas[i].getCerto()){
			titulo.setAttribute('class','erroRelatorio');
		}
		paragrafo = document.createElement('p');
		conteudoRelatorio.appendChild(titulo);
		if(opcoesErradas[i].getComentado()){
			paragrafo.innerHTML += opcoesErradas[i].getComentario().replace(/\n/g,'<br />');
			paragrafo.setAttribute('class','comentarioRelatorio');
		}
		conteudoRelatorio.appendChild(paragrafo);
	}
}

function resetarSeletores(){
	var seletorIdiomas = getSeletor('idioma');
	var seletorListas = getSeletor('lista');
	var seletorDelimitadores = getSeletor('delimitador');
	
	
	if(seletorListas.options.length <= 2){
		bloquearSeletor('lista',false);
	} else {
		seletorListas.options[0].selected = true;
	}
	
	
	if(seletorDelimitadores.options.length <= 2){
		bloquearSeletor('delimitador',false);
	} else {
		seletorDelimitadores.options[0].selected = true;
	}
	
	if(seletorIdiomas.options.length > 2){
		seletorIdiomas.selectedIndex = 0;
		bloquearSeletor('lista',false);
		bloquearSeletor('delimitador',false);
		seletorListas.options[0].selected = true;
		seletorDelimitadores.options[0].selected = true;
	}
}

function salvar(e){
	e.target.style.display = 'none';
}

function carregar(e){
	e.target.style.display = 'none';
}

function carregaCookie(nome){
	var cookies = document.cookie.split(';');
	for(var i = 0; i < cookies.length; i++){
		cookies[i] = cookies[i].split('=');
		if(trim(cookies[i][0].toLowerCase()) == nome.toLowerCase()){
			return cookies[i][1];
		}
	}
	return null;
}

function definirCookie(nome,valor,expira){
	if(expira != null && expira != 0){
		var data = new Date();
		data = data.getTime() + expira*24*60*60*1000;
		data = new Date(data);
		data = data.toUTCString();
		expira = ';expires=' + data;
	} else {
		expira = '';
	}
	document.cookie = nome + '=' + valor + expira;
}

function checaLogin(){
	var logado = carregaCookie('self_qa-logged');
	var end = new String(window.location);
	end = end.substring(end.search(/\?/)!=-1?end.search(/\?/):end.length,end.length);
	if(logado == 'false' || logado == null || logado == undefined){
		window.location = 'index.html' + end;
	}
}

function checaNavegador(){
	isIE = false;
	if(navigator.userAgent.search('MSIE') != -1){
		isIE = true;
	}
}

function carregarLocalizacoes(){
	var conexaoLocalizacoes;
	var localizacoes;
	// Cria um objeto XMLHttpRequest para pegar os dados do servidor
	if (window.XMLHttpRequest) {// Código para IE7+, Firefox, Chrome, Opera, Safari
		conexaoLocalizacoes = new XMLHttpRequest();
	}
	else {// Código para IE6, IE5
		conexaoLocalizacoes = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	// Define um eventListener para chamar a função que continuará a carregar os dados quando o xml for carregado.
	if(!isIE){conexaoLocalizacoes.addEventListener('load', continuarCargaLocalizacoes, false);}else{conexaoLocalizacoes.onreadystatechange = continuarCargaLocalizacoes;}
	
	// Checa se os arquivos de idiomas definidos no arquivo de configurações (vetor 'languages') existem
	for(var i = 0; i < languages.length; i++) {
		conexaoLocalizacoes.open('GET','./content/languages/' + idiomaSelecionado+ '.xml', false);
		conexaoLocalizacoes.send();
    }
	
	function continuarCargaLocalizacoes(){
		if(conexaoLocalizacoes.readyState == '4' && (conexaoLocalizacoes.status == '200' || conexaoLocalizacoes.status == '0')) {
			localizacoes = conexaoLocalizacoes.responseXML;			
			TITLE = localizacoes.getElementById('title').textContent;
			TITLE_DONE = localizacoes.getElementById('titleDone').textContent;
			PAGE_TITLE = localizacoes.getElementById('pageTitle').textContent;
			SHOW = localizacoes.getElementById('show').textContent;
			HIDE = localizacoes.getElementById('hide').textContent;
			ALL_SECTIONS = localizacoes.getElementById('allSections').textContent;
			ALL_HINTS = localizacoes.getElementById('allHints').textContent;
			ALL_COMMENTS = localizacoes.getElementById('allComments').textContent;
			HINT = localizacoes.getElementById('hint').textContent;
			COMMENT = localizacoes.getElementById('comment').textContent;
			CREATED_BY = localizacoes.getElementById('createdBy').textContent;
			VERSION = localizacoes.getElementById('version').textContent;
			//TIMER = localizacoes.getElementById('timer').textContent;
			//TIMER_START = localizacoes.getElementById('timerStart').textContent;
			//TIMER_RESET = localizacoes.getElementById('timerReset').textContent;
			//TIMER_PAUSE = localizacoes.getElementById('timerPause').textContent;
			//TIMER_CONTINUE = localizacoes.getElementById('timerContinue').textContent;
			FOR_TEXT = localizacoes.getElementById('forText').textContent;
			NOT_FOR_TEXT = localizacoes.getElementById('notForText').textContent;
			MAIN_MENU = localizacoes.getElementById('mainMenu').textContent;
			REPORT = localizacoes.getElementById('report').textContent;
			CONFIGURATIONS = localizacoes.getElementById('configurations').textContent;
			HELP = localizacoes.getElementById('help').textContent;
			CLOSE_BUTTONS = localizacoes.getElementById('closeButtons').textContent;
			BACK_BUTTONS = localizacoes.getElementById('backButtons').textContent;
			SELECTED_LANGUAGE = localizacoes.getElementById('selectedLanguage').textContent;
			SELECTED_LIST = localizacoes.getElementById('selectedList').textContent;
			SELECTED_DELIMITER = localizacoes.getElementById('selectedDelimiter').textContent;
			CHANGE_BUTTONS = localizacoes.getElementById('changeButtons').textContent;
			RESET_BUTTONS = localizacoes.getElementById('resetButtons').textContent;
			LANGUAGE = localizacoes.getElementById('language').textContent;
			LIST = localizacoes.getElementById('list').textContent;
			DELIMITER = localizacoes.getElementById('delimiter').textContent;
			SELECT = localizacoes.getElementById('select').textContent;
			TIP = localizacoes.getElementById('tip').textContent
			TIP_SELECT = localizacoes.getElementById('tipSelect').textContent;
			TIP_TEXT = localizacoes.getElementById('tipText').textContent;
			GENERATE_REPORT = localizacoes.getElementById('generateReport').textContent;
			CHANGE_CONFIGURATIONS = localizacoes.getElementById('changeConfigurations').textContent;
			SHORTCUTS = localizacoes.getElementById('shortcuts').textContent;
			SHORTCUT1 = localizacoes.getElementById('shortcut1').textContent;
			SHORTCUT2 = localizacoes.getElementById('shortcut2').textContent;
			SHORTCUT3 = localizacoes.getElementById('shortcut3').textContent;
			SHORTCUT4 = localizacoes.getElementById('shortcut4').textContent;
			SHORTCUT5 = localizacoes.getElementById('shortcut5').textContent;
			SHORTCUT6 = localizacoes.getElementById('shortcut6').textContent;
			SHORTCUT7 = localizacoes.getElementById('shortcut7').textContent;
			SHORTCUT8 = localizacoes.getElementById('shortcut8').textContent;
			SHORTCUT9 = localizacoes.getElementById('shortcut9').textContent;
			PERCENTAGE = localizacoes.getElementById('percentage').textContent;
			SHORTCUT_TEXT1 = localizacoes.getElementById('shortcutText1').textContent;
			SHORTCUT_EXAMPLE1 = localizacoes.getElementById('shortcutExample1').textContent;
		}
		
		document.getElementById('tit_pagina').innerHTML = PAGE_TITLE;
		
		document.getElementById('btn_secoesExibir').innerHTML = SHOW + ' ' + ALL_SECTIONS;
		document.getElementById('btn_secoesEsconder').innerHTML = HIDE + ' ' + ALL_SECTIONS;
		
		document.getElementById('btn_dicasExibir').innerHTML = SHOW + ' ' + ALL_HINTS;
		document.getElementById('btn_dicasEsconder').innerHTML = HIDE + ' ' + ALL_HINTS;
		
		document.getElementById('btn_comentariosExibir').innerHTML = SHOW + ' ' + ALL_COMMENTS;
		document.getElementById('btn_comentariosEsconder').innerHTML = HIDE + ' ' + ALL_COMMENTS;
		
		//document.getElementById('lbl_timer').innerHTML = TIMER;
		//document.getElementById('btn_inicar_timer').innerHTML = TIMER_START;
		//document.getElementById('btn_pausar_timer').innerHTML = TIMER_PAUSE;
		
		document.getElementById('lbl_criadoPor').innerHTML = CREATED_BY;
		document.getElementById('lbl_versao').innerHTML = VERSION;
		
		document.getElementById('lbl_idiomaSelecionado').innerHTML = SELECTED_LANGUAGE;
		document.getElementById('lbl_listaSelecionada').innerHTML = SELECTED_LIST;
		document.getElementById('lbl_delimitadorSelecionado').innerHTML = SELECTED_DELIMITER;
		
		document.getElementById('btn_mudar').innerHTML = CHANGE_BUTTONS;
		document.getElementById('btn_resetar').innerHTML = RESET_BUTTONS;
		
		document.getElementById('lbl_idioma').innerHTML = LANGUAGE;
		document.getElementById('lbl_lista').innerHTML = LIST;
		document.getElementById('lbl_delimitador').innerHTML = DELIMITER;
		
		document.getElementById('textoMenuDica').innerHTML = TIP_TEXT;
		document.getElementById('textoSelecionarMenuDica').innerHTML = TIP_SELECT;
		
		document.getElementById('btn_voltar_mnu_configuracoes').innerHTML = BACK_BUTTONS;
		document.getElementById('btn_fechar_mnu_configuracoes').innerHTML = CLOSE_BUTTONS;
		document.getElementById('btn_voltar_mnu_relatorio').innerHTML = BACK_BUTTONS;
		document.getElementById('btn_fechar_mnu_relatorio').innerHTML = CLOSE_BUTTONS;
		document.getElementById('btn_voltar_mnu_ajuda').innerHTML = BACK_BUTTONS;
		document.getElementById('btn_fechar_mnu_ajuda').innerHTML = CLOSE_BUTTONS;
		document.getElementById('btn_voltar_mnu_dica').innerHTML = BACK_BUTTONS;
		document.getElementById('btn_fechar_mnu_dica').innerHTML = CLOSE_BUTTONS;
		
		document.getElementById('btn_relatorio').innerHTML = GENERATE_REPORT;
		document.getElementById('btn_configuracoes').innerHTML = CHANGE_CONFIGURATIONS;
		document.getElementById('btn_ajuda').innerHTML = HELP;
		document.getElementById('btn_fechar').innerHTML = CLOSE_BUTTONS;
		
		document.getElementById('atalhos').innerHTML = SHORTCUTS;
		document.getElementById('atalho1').innerHTML = SHORTCUT1;
		document.getElementById('atalho2').innerHTML = SHORTCUT2;
		document.getElementById('atalho3').innerHTML = SHORTCUT3;
		document.getElementById('atalho4').innerHTML = SHORTCUT4;
		document.getElementById('atalho5').innerHTML = SHORTCUT5;
		document.getElementById('atalho6').innerHTML = SHORTCUT6;
		document.getElementById('atalho7').innerHTML = SHORTCUT7;
		document.getElementById('atalho8').innerHTML = SHORTCUT8;
		document.getElementById('atalho9').innerHTML = SHORTCUT9;
		
		document.getElementById('atalhoTexto1').innerHTML = SHORTCUT_TEXT1;
		document.getElementById('atalhoExemplo1').innerHTML = SHORTCUT_EXAMPLE1;
		
		document.getElementById('cmb_idioma').getElementsByTagName('select')[0].options[0].text = SELECT;
		document.getElementById('cmb_lista').getElementsByTagName('select')[0].options[0].text = SELECT;
		document.getElementById('cmb_delimitador').getElementsByTagName('select')[0].options[0].text = SELECT;
		
		if(PERCENTAGE.toLowerCase() == 'comma'){porcentagemCompleta.setDelimitadorDecimal(true)}else{porcentagemCompleta.setDelimitadorDecimal(false)}
	}
}


function criarMenus(){
	menus.push(new Menu('mnu_principal',MAIN_MENU));
	menus.push(new Menu('mnu_configuracoes',CONFIGURATIONS));
	menus.push(new Menu('mnu_relatorio',REPORT));
	menus.push(new Menu('mnu_ajuda',HELP));
	menus.push(new Menu('mnu_dica',TIP));
}

function recriarMenus(){
	menus[0].setNome(MAIN_MENU);
	menus[1].setNome(CONFIGURATIONS);
	menus[2].setNome(REPORT);
	menus[3].setNome(HELP);
	menus[4].setNome(TIP);
	
	menus[1].atualizaTitulo();
}

function testaConfigUrl(){
	var end = new String(window.location);
	end = end.substring(end.search(/\?/) + 1,end.length);
	if(end.search(/\?/)==-1){
		end = unescape(end);
		end = end.replace(/#/,'');
		var variaveis = end.split('&');
		for(var i = 0; i < variaveis.length; i++){
			variaveis[i] = variaveis[i].split('=');
			switch(variaveis[i][0]){
				case 'lang':
					var idioma = variaveis[i][1];
					definirCookie('idioma',idioma,1);
					while(carregaCookie('idioma') == null){
						continue;
					}
					break;
				case 'type':
					var lista = variaveis[i][1];
					definirCookie('lista',lista,1);
					break;
				case 'del': var delimitador = variaveis[i][1];
					definirCookie('delimitador',delimitador,1);
					break;
			}
		}
	}
}

function abreMenuPrincipal(){
		var temAberto;
		for(var i = 0; i < menus.length; i++) {
			if (menus[i].getId() == 'mnu_principal' && menus[i].getVisivel() == true){
				temAberto = true;
				break;
			}
		}
		if(temAberto){
			fecharTodosMenus();
		} else {
			abrirMenu('mnu_principal',false,false);
		}
}