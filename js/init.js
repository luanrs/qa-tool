﻿// Cria evento para carregar as informações e definir as variáves de ambiente
checaNavegador();

// Função que irá carregar a página
function inicializador() {
	
	testaConfigUrl();
	//checaLogin();

	definirVariaveisAmbiente();
	definirConfiguracaoPadrao();
	definirConfiguracaoCookie();
	
	carregarIdiomas();
	carregarLocalizacoes();
	
	if(idiomaSelecionado != '0'){
		if(idiomasCorrigido.length > 1){
			carregarListas();
			carregarDelimitadores();
		} else {
			carregarListas('carregamentoCompleto');
			carregarDelimitadores('carregamentoCompleto');
		}
		
	}
	if(loadOnStart && idiomaSelecionado != '0' && listaSelecionada != '0') {
		atualizarDescConfig();
		carregarConteudo();
		escreverConteudo();
	} else {
		document.getElementById('conteudo').innerHTML = 'Please select a list';
	}
	
	desativarBotao(document.getElementById('btn_mudar'));
	
	definirTitulo();
	
	//This will set all the event listeners
	if(!isIE){getSeletor('idioma').addEventListener('change', mudarIdioma, false);}else{getSeletor('idioma').onchange = mudarIdioma;}
	if(!isIE){getSeletor('lista').addEventListener('change', mudarLista, false);}else{getSeletor('lista').onchange = mudarLista;}
	if(!isIE){document.getElementById('btn_mudar').addEventListener('click', recarregarConteudo, false);}else{document.getElementById('btn_mudar').onclick = recarregarConteudo;}
	if(!isIE){document.getElementById('btn_fecharTopo').addEventListener('click', fecharTodosMenus, false);}else{document.getElementById('btn_fecharTopo').onclick = fecharTodosMenus;}
	if(!isIE){document.getElementById('btn_resetar').addEventListener('click', resetarSeletores, false);}else{document.getElementById('btn_resetar').onclick = resetarSeletores;}
	if(!isIE){document.getElementById('btn_configuracoes').addEventListener('click', function(){abrirMenu('mnu_configuracoes',true,true);}, false);}else{document.getElementById('btn_configuracoes').onclick = function(){abrirMenu('mnu_configuracoes',true,true);};}
	if(!isIE){document.getElementById('btn_fechar').addEventListener('click', fecharMenu, false);}else{document.getElementById('btn_configuracoes').onclick = fecharMenu;}
	document.getElementById('btn_salvar').style.display = 'none'; //addEventListener('click', salvar, false);
	document.getElementById('btn_carregar').style.display = 'none'; //.addEventListener('click', carregar, false);
	if(!isIE){document.getElementById('btn_relatorio').addEventListener('click', function(){carregarRelatorio(); abrirMenu('mnu_relatorio',true,true);}, false);}else{document.getElementById('btn_relatorio').onclick = function(){carregarRelatorio(); abrirMenu('mnu_relatorio',true,true);}}
	if(!isIE){document.getElementById('btn_ajuda').addEventListener('click', function(){abrirMenu('mnu_ajuda',true,true);}, false);}else{document.getElementById('btn_ajuda').onclick = function(){abrirMenu('mnu_ajuda',true,true);}}
	if(!isIE){document.getElementById('btn_voltar_mnu_configuracoes').addEventListener('click', fecharMenu, false);}else{document.getElementById('btn_voltar_mnu_configuracoes').onclick = fecharMenu;}
	if(!isIE){document.getElementById('btn_fechar_mnu_configuracoes').addEventListener('click', fecharTodosMenus, false);}else{document.getElementById('btn_fechar_mnu_configuracoes').onclick = fecharTodosMenus;}
	if(!isIE){document.getElementById('btn_voltar_mnu_relatorio').addEventListener('click', fecharMenu, false);}else{document.getElementById('btn_voltar_mnu_relatorio').onclick = fecharMenu;}
	if(!isIE){document.getElementById('btn_fechar_mnu_relatorio').addEventListener('click', fecharTodosMenus, false);}else{document.getElementById('btn_fechar_mnu_relatorio').onclick = fecharTodosMenus;}
	if(!isIE){document.getElementById('btn_voltar_mnu_ajuda').addEventListener('click', fecharMenu, false);}else{document.getElementById('btn_voltar_mnu_ajuda').onclick = fecharMenu;}
	if(!isIE){document.getElementById('btn_fechar_mnu_ajuda').addEventListener('click', fecharTodosMenus, false);}else{document.getElementById('btn_fechar_mnu_ajuda').onclick = fecharTodosMenus;}
	if(!isIE){document.getElementById('btn_fechar_mnu_dica').addEventListener('click', function(){fecharTodosMenus();
		var input = document.getElementById('cbx_dica').checked;
		if(input == true){
			definirCookie('hintOnStart','false',360);
		}	
	}, false);}else{document.getElementById('btn_fechar_mnu_dica').onclick = function(){fecharTodosMenus();
		var input = document.getElementById('cbx_dica').checked;
		if(input == true){
			definirCookie('hintOnStart','false',360);
		}
		}
	}
	if(!isIE){window.addEventListener('keydown', combinacaoTeclado, false);}else{window.onkeydown = combinacaoTeclado;}
	if(!isIE){document.getElementById('fundoNegro').addEventListener('click', fecharTodosMenus, false);}else{document.getElementById('fundoNegro').onclick = fecharTodosMenus;}
	
	if(!isIE){document.getElementById('btn_secoesExibir').addEventListener('click', function(e){expandirSecoes(true)}, false);}else{document.getElementById('btn_secoesExibir').onclick = function(e){expandirSecoes(true);}}
	if(!isIE){document.getElementById('btn_dicasExibir').addEventListener('click', function(e){expandirDicasComentarios('dica',true)}, false);}else{document.getElementById('btn_dicasExibir').onclick = function(e){expandirDicasComentarios('dica',true)}}
	if(!isIE){document.getElementById('btn_comentariosExibir').addEventListener('click', function(e){expandirDicasComentarios('comentario',true)}, false);}else{document.getElementById('btn_comentariosExibir').onclick = function(e){expandirDicasComentarios('comentario',true)}}
	
	if(!isIE){document.getElementById('btn_secoesEsconder').addEventListener('click', function(e){expandirSecoes(false)}, false);}else{document.getElementById('btn_secoesEsconder').onclick = function(e){expandirSecoes(false)}}
	if(!isIE){document.getElementById('btn_dicasEsconder').addEventListener('click', function(e){expandirDicasComentarios('dica',false)}, false);}else{document.getElementById('btn_dicasEsconder').onclick = function(e){expandirDicasComentarios('dica',false)}}
	if(!isIE){document.getElementById('btn_comentariosEsconder').addEventListener('click', function(e){expandirDicasComentarios('comentario',false)}, false);}else{document.getElementById('btn_comentariosEsconder').onclick = function(e){expandirDicasComentarios('comentario',false)}}
		
	//Criar instância para todos os menus
	criarMenus();
	
	if(hintOnStart == true && carregaCookie('hintOnStart') != 'false'){abrirMenu('mnu_dica',true,false);}
}

function definirVariaveisAmbiente(){
	// Define as variáveis de ambiente
	idiomaSelecionado = '0';
	listaSelecionada = '0';
	delimitadorSelecionado = '0';

	nomeIdiomaSelecionado = '';
	nomeListaSelecionada = '';
	nomeDelimitadorSelecionado = '';

    // Vetores corrigidos, que irão conter apenas opções existentes e sem repetições
	idiomasCorrigido = new Array;
	listasCorrigido = new Array;
	delimitadoresCorrigido = new Array;
	
	// Vetores que irão conter as opcoes, secoes e menus
	secoes = new Array;
	opcoes = new Array;
	menus = new Array;
	
	expandidoSecoes = false;
	expandidoDicas = false;
	expandidoComentarios = false;
		
	porcentagemCompleta = new Porcentagem(0,true);
}

// Essa função irá checar se existe alguma variável padrão configurada, se sim, irá configurar as variáveis de
// ambiente para serem utilizadas corretamente
function definirConfiguracaoPadrao()
{
	// Esse laço irá testar todas as opções do vetor de idiomas (languages) e comparar com a variável
	// padrão configurada, se encontra algum valor igual, irá definir a variável idiomaSelecionado
	for(var i = 0; i < languages.length; i++) {
		if(defaultLanguage.toLowerCase() == languages[i].toLowerCase()) {
			idiomaSelecionado = defaultLanguage;
			break;
		}
	}
	
	// Esse laço irá testar todas as opções do vetor de listas (lists) e comparar com a variável
	// padrão configurada, se encontra algum valor igual, irá definir a variável listaSelecionada
	for(var i = 0; i < lists.length; i++) {
		if(defaultList.toLowerCase() == lists[i].toLowerCase()) {
			listaSelecionada = defaultList;
			break;
		}
	}
	
	// A variável delimitadorSelecionado não é testado aqui pois seus valores são carregados dinamicamente mais tarde
	delimitadorSelecionado = defaultDelimiter;
}

// Função que carregará o seletor de idiomas.
function carregarIdiomas() {
	// Variável que conterá o objeto XMLHttpRequest que será usado para obter o XML com os idiomas
	var conexaoIdiomas;
	
	var xmlIdiomas;
	var nomeIdiomaTemporario;
	var codigoIdiomaTemporario;
	var idiomaPadraoOk = false;
	var idioma;
	var idiomaRepetido;
	var opcaoSeletorIdioma;
	
	// Limpa o seletor de idioma e depoir carrega ele para ser usado posteriormente		
	var seletorIdioma = limpaSeletor('idioma');
	
	// Cria um objeto XMLHttpRequest para pegar os dados do servidor
	if (window.XMLHttpRequest) {// Código para IE7+, Firefox, Chrome, Opera, Safari
		conexaoIdiomas = new XMLHttpRequest();
	}
	else {// Código para IE6, IE5
		conexaoIdiomas = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	// Define um eventListener para chamar a função que continuará a carregar os dados quando o xml for carregado.
	if(!isIE){conexaoIdiomas.addEventListener('load', continuarCargaIdiomas, false);}else{conexaoIdiomas.onreadystatechange = continuarCargaIdiomas;}
	
	// Checa se os arquivos de idiomas definidos no arquivo de configurações (vetor 'languages') existem
	for(var i = 0; i < languages.length; i++) {
		conexaoIdiomas.open('GET','./content/languages/' + languages[i] + '.xml', false);
		conexaoIdiomas.send();
    }

    // Se em nenhum momento a o idioma padrão foi carregado, configura-se o valor para 0
    if (!idiomaPadraoOk) { idiomaSelecionado = '0';}
	
	// Após testar todos as opções de idioma e criar o veto de idiomas corrigido, pegar o seletor de idiomas e
	// adicionar as opções
	if (seletorIdioma.disabled == true) {seletorIdioma.removeAttribute('disabled');}
	switch(idiomasCorrigido.length) {
		case 1:
			// Caso exista apenas um elemento no vetor, ele será adicionar como selecionad e o comboBox será bloqueado
			adicionarOpcaoSeletor(seletorIdioma, idiomasCorrigido[0].getCodigo(), idiomasCorrigido[0].getNome(), true);
			idiomaSelecionado = idiomasCorrigido[0].getCodigo();
		case 0:
			// Caso não exista nenhum elemento no vetor, bloquear o comboBox
			seletorIdioma.setAttribute('disabled', 'disabled');
			break;
		default:
			// Em qualquer outro caso, adicionar a opção no final
			for (j = 0; j < idiomasCorrigido.length; j++) {
				adicionarOpcaoSeletor(seletorIdioma, idiomasCorrigido[j].codigo, idiomasCorrigido[j].nome, false);
			}
			break;
	}
	
	function continuarCargaIdiomas() {
		if(conexaoIdiomas.readyState == '4' && (conexaoIdiomas.status == '200' || conexaoIdiomas.status == '0')) { // Só continuará se a conexão tiver se completado e com status 200 (ok)
			// Pegar o conteúdo do arquivo xml e depois carregar as infomações de código de idioma e nome do idioma
			xmlIdiomas = conexaoIdiomas.responseXML;
			nomeIdiomaTemporario = xmlIdiomas.getElementsByTagName('textLocalizations')[0].getAttribute('language');
			codigoIdiomaTemporario = xmlIdiomas.getElementsByTagName('textLocalizations')[0].getAttribute('languageCode');

			// Testa se o idioma carregado é o idioma padrão, caso sim, define o nome do Idioma selecionad por padrão
            // Caso em nenhum momento o idioma padrão seja encontrado, redefine a variável para 0
			if(codigoIdiomaTemporario.toLowerCase() == idiomaSelecionado.toLowerCase()){
			    nomeIdiomaSelecionado = nomeIdiomaTemporario;
			    idiomaPadraoOk = true;
			}
			
			// Agora irá criar um objeto idioma com as informações carregadas
			idioma = new Idioma(codigoIdiomaTemporario,nomeIdiomaTemporario);
			
			// Após criar o objeto idioma, irá adicioná-lo no vetor com os idiomas corrigidos
			// Primeiro irá checar se o vetor está vazio, caso sim, irá adicionar o objeto diretamente
			if(idiomasCorrigido.length == 0) {
				idiomasCorrigido.push(idioma);
            } else { // Caso contrário irá vasculhar o vetor para verificar se o codigo já existe
                idiomaRepetido = false;
                for (j = 0; j < idiomasCorrigido.length; j++) {
                    // Caso exista, sairá do laço
				    if (idiomasCorrigido[j].codigo.toLowerCase() == idioma.codigo.toLowerCase()) {break; idiomaRepetido = true;} 
				}
				
                //Se não encontrar, irá adicionar ao vetor de idiomas corrigido
				if(!idiomaRepetido) {
				    idiomasCorrigido.push(idioma);
				}
			}
		}
	}
}

function definirConfiguracaoCookie() {
	var idioma = carregaCookie('idioma');
	var lista = carregaCookie('lista');
	var delimitador = carregaCookie('delimitador');
	if(idioma != null && idioma != ''){
		idiomaSelecionado = idioma;
		definirCookie('idioma','',0);
	}
	if(lista != null && lista != ''){
		listaSelecionada = lista;
		definirCookie('lista','',0);
	}
	if(delimitador != null && delimitador != ''){
		delimitadorSelecionado = delimitador;
		definirCookie('delimitador','',0);
	}	
}