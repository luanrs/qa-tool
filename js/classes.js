// Classe para os Idiomas gerados
function Idioma(codigo, nome) {
    // Definição dos atributos da classe
	this.codigo;
	this.nome;
	
	// Funções setters
	this.setCodigo = function(codigo) {this.codigo = codigo;}	
	this.setNome = function(nome) {this.nome = nome;}
	
	// Funções getters
	this.getCodigo = function() {return this.codigo;}
	this.getNome = function() {return this.nome;}
	
	// Inicio do método construtor
	this.setCodigo(codigo);
	this.setNome(nome);
}

// Classe para as Listas geradas
function Lista(idioma, tipo, nome) {
	// Definição dos atributos da classe
	this.idioma = idioma;
	this.tipo = tipo;
	this.nome = nome;
	
	// Funções setters
	this.setIdioma = function(idioma) {this.idioma = idioma;}
	this.setTipo = function(tipo) {this.tipo = tipo;}
	this.setNome = function(nome) {this.nome = nome;}
	
	// Funções getters
	this.getIdioma = function() {return this.idioma;}
	this.getTipo = function() {return this.tipo;}
	this.getNome = function() {return this.nome;}
	
	// Inicio do método construtor
	this.setIdioma(idioma);
	this.setTipo(tipo);
	this.setNome(nome);
}

// Classe para os delimitadores gerados
function Delimitador(codigo, nome) {
	// Definição dos atributos da classe
	this.codigo;
	this.nome;
	
	// Funções setters
	this.setCodigo = function(codigo) {this.codigo = codigo;}
	this.setNome = function(nome) {this.nome = nome;}
	
	// Funções getters
	this.getCodigo = function() {return this.codigo;}
	this.getNome = function() {return this.nome;}
	
	// Inicio do método construtor
	this.setCodigo(codigo);
	this.setNome(nome);
}

// Os objetos dessa classe conterão as porcentagens do sistema, assim como exibirão os resultados
function Porcentagem(valor,decimal) {
	// Definição dos atributos da classe
	this.valor;
	// Esse atributo irá definir o delimitador decimal, caso seja verdadeiro, será uma virgula, caso falso, um ponto.
	this.delimitadorDecimal;
	
	// Funções setters
	this.setValor = function(valor) {this.valor = parseFloat(valor)?parseFloat(valor):0;}
	this.setDelimitadorDecimal = function(delimitador) {
		if(delimitador || delimitador == 'virgula'){this.delimitadorDecimal = true;} else {this.delimitadorDecimal = false;}
	}
	
	// Funções getters
	// Esse método irá retornar o valor do objeto em uma string, com o caractere % no final
	this.getPorcentagem = function() {
		// Primeiro checar se o resultado irá utilizar virgula ou ponto decimal, em ambos os casos
		// irá truncar o valor da porcentagem para duas casas decimais
		if(this.delimitadorDecimal == true) {
			// Se verdadeiro, trocar o número de ponto para virgula e adicionar o símbolo %
			var numero = parseFloat(this.valor*100).toFixed(2);
			var numeroString = numero.toString();
			numeroString = numeroString.replace('.',',');
			return numeroString + '%';
		} else {
			// Se falso, apenas adicionar o símbolo %
			return parseFloat(this.valor*100).toFixed(2).toString() + '%';
		}
	}
	
	// Inicio do método construtor
	this.setValor(valor);
	this.setDelimitadorDecimal(decimal);
}

// Essa  classe irá conter os dados das seções das listas
function Secao(id, nome, visivel, mae) {
	// Definição dos atributos da classe
	this.id;
	this.nome;
	this.visivel;
	this.opcoes;
	this.mae;
	
	// Funções setters
	this.setId = function(id) {this.id = id;}
	this.setNome = function(nome) {this.nome = nome;}
	this.setVisivel = function(visivel) {
		if(visivel){
			this.visivel = true;
		} else {
			this.visivel = false;
		}
	}
	this.setMae = function(mae) {this.mae = mae;}
	
	// Funções getters
	this.getId = function() {return this.id;}
	this.getNome = function() {return this.nome;}
	this.getVisivel = function() {return this.visivel;}
	this.getOpcoes = function() {return this.opcoes;}
	this.getMae = function() {return this.mae;}
	this.getPosicaoOpcao = function(id) {
		return this.procuraOpcao(id,'posicao');
	}
	// Diferente do método getOpcoes, que retorna o array com todas as opções
	// Esse método irá retornar uma opções específica do Array, buscando pela posição ou pelo id
	this.getOpcao = function(id) {
		if(isNaN(id)) {return this.procuraOpcao(id,'opcao');} 
		else {
			if(id < this.opcoes.length) {return this.opcoes[id];} else {return null;}
		}
	}
	
	// Outras funções
	// Esse método irá procurar por uma opção pelo id e irá retornar o objeto opção, ou a posição
	// dela no array
	this.procuraOpcao = function(id,modoDeRetorno) {
		for(i = 0; i < this.opcoes.length; i++) {
			if(this.opcoes[i].getId() == id){
				if(modoDeRetorno == 'posicao') {return i;} else {return this.opcoes[i];}
			}
		}
		return null;
	}
		
	//Essa função serve para inverter o atributo visivel do objeto
	this.trocaVisibilidade = function()
	{
		if(this.visivel == true) {
			this.esconderSecao();
		} else {
			this.mostrarSecao();
		}
	}
	
	// Adiciona uma opção ao array de opções
	this.adicionarOpcao = function(opcao){this.opcoes.push(opcao);}
	
	// Remove uma opção específica do array, dando o id da opção
	this.removerOpcao = function(opcao){
		var temOpcao = procuraOpcao(opcao,'posicao');
		if(temOpcao != null){
			for(i = temOpcao; i < this.opcoes.length - 1; i++)
			{
				this.opcoes[i] = this.opcoes[i+1];
			}
		}
	}
	
	// Esconde a seção
	this.esconderSecao = function(){
		this.setVisivel(false);
		var conteudoSecao = document.getElementById(this.id + '_conteudo')
		conteudoSecao.style.display = 'none';
	}
	
	// Mostra a seção
	this.mostrarSecao = function(){
		this.setVisivel(true);
		var conteudoSecao = document.getElementById(this.id + '_conteudo')
		conteudoSecao.style.display = 'block';
	}
	
	// Inicio do método construtor
	this.setId(id);
	this.setNome(nome);
	this.setVisivel(visivel);
	this.opcoes = new Array();
	this.setMae(mae);
}

// Esse objeto irá guardar as opções das listas
function Opcao(id, nome, dica, certo, secao) {
	// Definição dos atributos da classe
	this.id;
	this.nome;
	this.certo;
	this.dica;
	this.comentado;
	this.comentario;
	this.status;
	this.dicaVisivel;
	this.comentarioVisivel;
	this.secao;
	
	// Funções setters
	this.setId = function(id) {this.id = id;}
	this.setNome = function(nome) {this.nome = nome;}
	this.setCerto = function(certo){
		if(certo){
			this.certo = true;
		} else {
			this.certo = false;
		}
		this.atualizarStatus();
	}
	this.setDica = function(dica) {this.dica = dica;}
	this.setComentario = function(comentario) {
		comentario = trim(comentario);
		this.comentario = comentario;
		if(comentario != ''){			
			this.comentado = true;			
		} else {
			this.comentado = false;
		}
		this.atualizarStatus();
	}
	this.setDicaVisivel = function(dicaVisivel) {this.dicaVisivel = dicaVisivel;}
	this.setComentarioVisivel = function(comentarioVisivel) {this.comentarioVisivel = comentarioVisivel;}
	this.setSecao = function(secao) {this.secao = secao;}
	
	// Funções getters
	this.getId = function() {return this.id;}
	this.getNome = function() {return this.nome;}
	this.getCerto = function() {return this.certo;}
	this.getDica = function() {return this.dica;}
	this.getComentado = function() {return this.comentado;}
	this.getComentario = function() {return this.comentario;}
	this.getStatus = function() {return this.status;}
	this.getDicaVisivel = function() {return this.dicaVisivel;}
	this.getComentarioVisivel = function() {return this.comentarioVisivel;}
	this.getSecao = function() {return this.secao;}
	
	// Outras funções
	//  Esse método irá remover o comentário
	this.removerComentario = function() {
		this.comentario = '';
		this.temComentario = false;
		this.atualizarStatus();
	}
		
	this.atualizarStatus = function(){
		if(this.comentado){
			if(this.certo){
				this.status = 3;
			} else {
				this.status = 2;
			}
		}
		else {
			if(this.certo){
				this.status = 1;
			} else {
				this.status = 0;
			}
		}
	}
	
	this.trocarCerto = function(){
		if(this.certo == true){
			setCerto(false);
		} else {
			setCerto(true);
		}
	}
	
	this.mostrarDica = function(){		if(trim(this.dica) != ''){
			var elementoDica = document.getElementById(this.id + '_dica');
			elementoDica.style.display = 'block';
			this.dicaVisivel = true;		}
	}
	
	this.mostrarComentario = function(){
		var elementoComentario = document.getElementById(this.id + '_comentario');
		elementoComentario.style.display = 'block';
		this.comentarioVisivel = true;
	}
	
	this.esconderDica = function(){		if(trim(this.dica) != ''){
			var elementoDica = document.getElementById(this.id + '_dica');
			elementoDica.style.display = 'none';
			this.dicaVisivel = false;		}
	}
	
	this.esconderComentario = function(){
		var elementoComentario = document.getElementById(this.id + '_comentario');
		elementoComentario.style.display = 'none';
		this.comentarioVisivel = false;
	}
	
	this.setId(id);
	this.setNome(nome);
	this.setDica(dica);
	this.setCerto(certo);
	this.setDicaVisivel(false);
	this.setComentarioVisivel(false);
	this.setSecao(secao);
}

function Menu(id, nome){
	this.id;
	this.nome;
	this.visivel = false;
	
	this.setId = function(id){this.id = id;}
	this.setNome = function(nome){this.nome = nome;}
	this.setVisivel = function(visivel){this.visivel = visivel;}
	
	this.getId = function(){return this.id;}
	this.getNome = function(){return this.nome;}
	this.getVisivel = function(){return this.visivel;}
	
	this.exibirMenu = function(){
		var menu = this.selecionarMenu();
		var menuTopo = document.getElementById('mnu_topo');
		var menuBase = document.getElementById('mnu_base');
		var titulo = this.selecionarTitulo();
		
		titulo.innerHTML = this.nome;
		
		menu.style.display = 'block';
		menuTopo.style.display = 'block';
		menuBase.style.display = 'block';
		
		this.visivel = true;
		menu.style.top = innerHeight/2 - menu.clientHeight/2;
		menu.style.left = innerWidth/2 - menu.clientWidth/2;
		
		menuTopo.style.top = innerHeight/2 - menu.clientHeight/2 - 15;
		menuTopo.style.left = innerWidth/2 - menu.clientWidth/2;
		
		menuBase.style.top = innerHeight/2 + menu.clientHeight/2 -1;
		menuBase.style.left = innerWidth/2 - menu.clientWidth/2;
	}
	
	this.esconderMenu = function(){
		var menu = this.selecionarMenu();		
		menu.style.display = 'none';		
		this.visivel = false;
	}
	
	this.selecionarMenu = function(){
		var menu = document.getElementById(this.id);
		return menu;
	}
	
	this.selecionarTitulo = function() {
		var titulo = document.getElementById('tit_' + this.id);
		return titulo;
	}
	
	this.fecharVisivel = function(){
		var botao = document.getElementById('btn_fechar_' + this.id);
		var botaoCorner = getNextSinbling(botao);
		botao.style.display = 'inline-block';
		botaoCorner.style.display = 'inline-block';
	}
	
	this.fecharInvisivel = function(){
		var botao = document.getElementById('btn_fechar_' + this.id);
		var botaoCorner = getNextSinbling(botao);
		botao.style.display = 'none';
		botaoCorner.style.display = 'none';
	}
	
	this.voltarVisivel = function(){
		var botao = document.getElementById('btn_voltar_' + this.id);
		var botaoCorner = getNextSinbling(botao);
		botao.style.display = 'inline-block';
		botaoCorner.style.display = 'inline-block';
	}
	
	this.voltarInvisivel = function(){
		var botao = document.getElementById('btn_voltar_' + this.id);
		var botaoCorner = getNextSinbling(botao);
		botao.style.display = 'none';
		botaoCorner.style.display = 'none';
	}
	
	this.atualizaTitulo = function(){
		var titulo = document.getElementById('tit_' + this.id);
		titulo.innerHTML = this.nome;
	}
	
	this.setId(id);
	this.setNome(nome);
}